import React, { useEffect } from "react";
import axios from "axios";
import XMLParser from "react-xml-parser";
import logo from "../../assets/group.png";
import playground from "../../assets/bitmap.jpg";
import group1 from "../../assets/group2.png";
import group2 from "../../assets/group-2.png";
import subsitute1 from "../../assets/subsitute/group-2.png";
import player from "../../assets/group-3-copy-20.png";
import "./_summary.scss";

function Summary() {
  useEffect(() => {
    axios
      .get("https://static.yinzcam.com/interviews/web/api/match1.xml", {
        "Content-Type": "application/xml; charset=utf-8",
      })
      .then(function (response) {
        const jsonDataFromXml = new XMLParser().parseFromString(response.data);
        console.log("OOOOOOOOOO", jsonDataFromXml);
        console.log("***********", jsonDataFromXml.getElementsByTagName("Id"));
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  return (
    <>
      <div className="row summary">
        <div className="row col-md-12">
          <div className="col-md-3 logo">
            <img src={logo} />
          </div>
          <div className="col-md-9">
            <div className="main-header">MATCH SUMMARY</div>
            <div className="sub-header">
              <span className="match-venue">22/8/2021</span>
              <span className="match-venue">5:00 PM</span>
              <span className="match-venue">CITY STADIUM</span>
            </div>
          </div>
        </div>
        <div className="row col-md-12 content">
          <div className="col-md-2 team-flag">
            <div>
              <img src={group1} />
            </div>
            <div>
              <img src={group2} />
            </div>
          </div>
          <div className="col-md-6">
            <img className="playground" src={playground} />
            <div className=" team1">
              <img className="team1-align" src={player} />
            </div>
            <div className=" team2">
              <img className="team2-align" src={player} />
            </div>
          </div>
          <div className="col-md-4 summary__substitues">
            <div className="row col-md-12">
              <div className="col-md-4 team1">
                <img src={subsitute1} />
                <div>team11</div>
                <div>team11</div>
                <div>team11</div>
                <div>team11</div>
              </div>
              <div className="col-md-4 middle">substitues</div>
              <div className="col-md-4 team2">
                <img src={subsitute1} />
                <div>team22</div>
                <div>team22</div>
                <div>team22</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Summary;
