import React from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Summary } from "./components";
import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route exact path="/summary" element={<Summary />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
